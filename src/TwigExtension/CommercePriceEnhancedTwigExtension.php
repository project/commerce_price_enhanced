<?php

namespace Drupal\commerce_price_enhanced\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig extension with some useful functions and filters.
 */
class CommercePriceEnhancedTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('number_part', [$this, 'getNumberPart']),
      new TwigFilter('fractional_part', [$this, 'getFractionalPart']),
    ];
  }

  /**
   * Get number part of price.
   */
  public static function getNumberPart(string $decimal_string) {
    preg_match('/^(?:[^\d]+)?([\d,]+)/', $decimal_string, $matches);
    $number_part = $matches[1];
    return $number_part;
  }

  /**
   * Get fractional part of price.
   */
  public static function getFractionalPart(string $decimal_string) {
    preg_match('/(\.\d+)(?:[^\d]+)?$/', $decimal_string, $matches);
    $fractional_part = $matches[0];
    return $fractional_part;
  }

}
